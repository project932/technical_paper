# Hostion
JavaScript Hoisting refers to the process whereby the interpreter appears to move the declaration of functions, variables or classes to the top of their scope, prior to execution of the code.
eg
```
function sayHello()
{
console.log("hello");
}
var i=0-sayHello();
console.log(i);
```
### output
```
Hello 10
```