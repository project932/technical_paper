# JavaScript supports different kinds of loops:

```
           1. for loop 
           2. for...in loop
           3. for...of loop
           4. while loop
           5. do/while loop
```

### 1. for loop 

for loop provides a concise way of writing the loop structure. Its commonly used to run code a set number of times. 

```
Syntax:

for (initialization; condition; increment/ decrement)
{
    // statement
}
```
```
eg - 
for (let i =0; i < 9; i++ )
{
    console.log(i);
}
```

### 2. for...in loop

The for...in statement combo iterates over teh properties of an object.

```
Syntax-

for (let x in object)
{
    //statement
}

```
```
eg-

const student = {
    name: 'pankaj',class:12,age: 20
};
for (let key in student )
{
    console.log('${key} => ${student[key]}');
    }
}
```
### 3. for...of loop

The JavaScript for in statement loops through the properties of an Object.

```
Syntax-
for (let key in object) {
  // statement
}
```
 ```
 eg-

 const num = [10, 20, 30];
 for (const value of num)
 {
    console.log(value);
 }
```
 ### 4. while loop

 The while loop loops through a block of code as long as a specified condition is true.

```
 Syntax-
 while (condition) {
  // statement
}
```
```
eg-
while (i < 10) {
  num += "The number is " + i;
  i++;
}
```

### 5. do/while loop

The do...while statements combo defines a code block to be executed once, and repeated as long as a condition is true.

```
Syntax:
do {
  //statement
}
while (condition);
```
```
eg-

let i = 1;
const n = 5;

do {
    console.log(i);
    i++;
} while(i <= n);
```